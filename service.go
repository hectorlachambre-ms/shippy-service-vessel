package main

import (
	"context"

	pb "gitlab.com/hectorlachambre-ms/shippy-service-vessel/proto/vessel"
)

// Service is our grpc service handler
type Service struct {
	repo IRepository
}

// FindAvailable returns Vessel that respect specifications
func (s *Service) FindAvailable(ctx context.Context, req *pb.Specification, res *pb.Response) error {

	// Find the next available vessel
	vessel, err := s.repo.FindAvailable(req)
	if err != nil {
		return err
	}

	// Set the vessel as part of the response message type
	res.Vessel = vessel
	return nil
}
