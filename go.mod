module gitlab.com/hectorlachambre-ms/shippy-service-vessel

go 1.14

require (
	github.com/golang/protobuf v1.3.5
	github.com/micro/go-micro/v2 v2.2.0
)
