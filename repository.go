package main

import (
	"errors"

	pb "gitlab.com/hectorlachambre-ms/shippy-service-vessel/proto/vessel"
)

// IRepository is contract referencing repository's action
type IRepository interface {
	FindAvailable(*pb.Specification) (*pb.Vessel, error)
}

// Repository implementation for Vessels
type Repository struct {
	vessels []*pb.Vessel
}

// FindAvailable - checks a specification against a map of vessels,
// if capacity and max weight are below a vessels capacity and max weight,
// then return that vessel.
func (repo *Repository) FindAvailable(spec *pb.Specification) (*pb.Vessel, error) {
	for _, vessel := range repo.vessels {
		if spec.Capacity <= vessel.Capacity && spec.MaxWeight <= vessel.MaxWeight {
			return vessel, nil
		}
	}
	return nil, errors.New("No vessel found by that spec")
}
