package main

import (
	"fmt"

	"github.com/micro/go-micro/v2"
	pb "gitlab.com/hectorlachambre-ms/shippy-service-vessel/proto/vessel"
)

func main() {

	srv := micro.NewService(
		micro.Name("shippy.service.vessel"),
	)

	srv.Init()

	vessels := []*pb.Vessel{
		&pb.Vessel{Id: "vessel001", Name: "Boaty McBoatface", MaxWeight: 200000, Capacity: 500},
	}
	repository := &Repository{vessels}

	service := &Service{repository}

	// Register our implementation with
	pb.RegisterVesselServiceHandler(srv.Server(), service)

	if err := srv.Run(); err != nil {
		fmt.Println(err)
	}
}
